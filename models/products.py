from sqlalchemy import Column, Integer, String, Text, DECIMAL, DateTime, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from .categories import Category, Base

class Product(Base):
    __tablename__ = 'products'

    id = Column(Integer, primary_key=True)
    title = Column(String(255), nullable=True)
    price = Column(DECIMAL(10, 2), nullable=True)
    description = Column(Text, nullable=True)
    images = Column(Text, nullable=True)
    creationAt = Column(DateTime, nullable=False, server_default='CURRENT_TIMESTAMP')
    updatedAt = Column(DateTime, nullable=False, server_default='0000-00-00 00:00:00')
    category_id = Column(Integer, ForeignKey('categories.id'))

    category = relationship(Category, back_populates="products")