from models.categories import Category as CategoryModel
from models.products import Product as ProductModel


class Product_Service():

    def __init__(self, db) -> None:
        self.db = db

    def get_all_product(self):
        result = self.db.query(ProductModel).all()
        return result
    
    def get_product(self, id: int):
        result = self.db.query(ProductModel).filter(ProductModel.id == id).first()
        return result
    
    def create_product(self, product: ProductModel):
        self.db.add(product)
        self.db.commit()
        self.db.refresh(product)
        return

    def get_products_by_category(self, category_name: str):
        category = self.db.query(CategoryModel).filter_by(name=category_name).first()
        if category: 
            result = self.db.query(ProductModel).filter(ProductModel.category_id == category.id).all()
        else:
            result = []
        return result
    
    def update_product(self, id: int, product: ProductModel):
        result = self.db.query(ProductModel).filter(ProductModel.id == id).first()
        result.title = product.title
        result.price = product.price
        result.description = product.description
        result.images = product.images
        result.updatedAt = product.updatedAt
        result.category_id = product.category_id
        self.db.commit()
        return

    def delete_product(self, id: int):
        result = self.db.query(ProductModel).filter(ProductModel.id == id).first()
        self.db.delete(result)
        self.db.commit()
        return