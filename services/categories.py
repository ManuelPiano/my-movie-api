from models.categories import Category as CategoriasModel

class Categorias_Service():
    
        def __init__(self, db) -> None:
            self.db = db
    
        def get_all_categorias(self):
            result = self.db.query(CategoriasModel).all()
            return result
        
        def get_categoria(self, id: int):
            result = self.db.query(CategoriasModel).filter(CategoriasModel.id == id).first()
            return result
        
        def create_categoria(self, categoria: CategoriasModel):
            self.db.add(categoria)
            self.db.commit()
            self.db.refresh(categoria)
            return
    
        def update_categoria(self, id: int, categoria: CategoriasModel):
            result = self.db.query(CategoriasModel).filter(CategoriasModel.id == id).first()
            result.name = categoria.name
            result.image = categoria.image
            result.updatedAt = categoria.updatedAt
            self.db.commit()
            return
    
        def delete_categoria(self, id: int):
            result = self.db.query(CategoriasModel).filter(CategoriasModel.id == id).first()
            self.db.delete(result)
            self.db.commit()
            return