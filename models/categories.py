from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship



Base = declarative_base()

class Category(Base):
    __tablename__ = 'categories'

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=True)
    image = Column(String(255), nullable=True)
    creationAt = Column(DateTime, nullable=False, server_default='CURRENT_TIMESTAMP')
    updatedAt = Column(DateTime, nullable=False, server_default='0000-00-00 00:00:00')

    products = relationship("Product", back_populates="category")
