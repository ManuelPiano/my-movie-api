from fastapi import Depends, FastAPI
from fastapi.responses import HTMLResponse
from config.database import engine, Base
from middlewares.error_handler import errorHandler
from routers.products import products_router
from routers.user import user_router
from routers.categories import categories_router
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

origins = [
    "http://localhost:5173",
]
app.title = "Detallitos para vos"
app.version = "0.0.1"

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

Base.metadata.create_all(bind=engine)
app.add_middleware(errorHandler)
app.include_router(products_router)
app.include_router(user_router)
app.include_router(categories_router)


@app.get('/', tags=['home'])
def message():
    return HTMLResponse('<h1>Hello world</h1>')

