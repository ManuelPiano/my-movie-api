from fastapi import APIRouter, Query, Path, Depends
from typing import Optional, List
from config.database import Sesion
from models.categories import Category as CategoriasModel
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JwtBearer
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from services.categories import Categorias_Service
from datetime import datetime

categories_router = APIRouter()

class categorias(BaseModel):
    id: Optional[int] = None
    name: str = Field(min_length=5, max_length=15)
    image: str = Field
    creationAt: datetime = Field(default_factory=datetime.now)
    updatedAt: datetime = Field(default_factory=datetime.now)
    class Config:
        schema_extra = {
            "example": {
                "id": 1,
                "name": "Donas categoria",
                "image": "Descripción de la categoria"
            }
        }

@categories_router.get('/categorias', tags=['categorias'], response_model=List[categorias], status_code=200)
def get_categoria() -> List[categorias]:
    db = Sesion()
    result = Categorias_Service(db).get_all_categorias()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@categories_router.get('/categorias/{id}', tags=['categorias'], response_model=categorias)
def get_categoria(id: int = Path(ge=1, le=2000)) -> categorias:
    db = Sesion()
    result = Categorias_Service(db).get_categoria(id)
    if not result:
         return JSONResponse(status_code=404, content={"message": "No se ha encontrado la categoria"})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@categories_router.post('/categorias', tags=['categorias'], response_model=dict, status_code=201)
def create_categoria(categoria: categorias) -> dict:
    db = Sesion()
    Categorias_Service(db).create_categoria(CategoriasModel(**categoria.dict()))
    return JSONResponse(status_code=201, content={"message": "Se ha registrado la categoria"})

@categories_router.put('/categorias/{id}', tags=['categorias'], response_model=dict, status_code=200)
def update_categoria(id: int, categoria: categorias) -> dict:
    db = Sesion()
    result = Categorias_Service(db).get_categoria(id)
    if not result:
        return JSONResponse(status_code=404, content={"message": "No se ha encontrado la categoria"})
    Categorias_Service(db).update_categoria(id, CategoriasModel(**categoria.dict()))
    return JSONResponse(status_code=200, content={"message": "Se ha actualizado la categoria"})

@categories_router.delete('/categorias/{id}', tags=['categorias'], response_model=dict, status_code=200)
def delete_categoria(id: int) -> dict:
    db = Sesion()
    result = Categorias_Service(db).get_categoria(id)
    if not result:
        return JSONResponse(status_code=404, content={"message": "No se ha encontrado la categoria"})
    Categorias_Service(db).delete_categoria(id)
    return JSONResponse(status_code=200, content={"message": "Se ha eliminado la categoria"})