from fastapi import APIRouter, Query, Path, Depends
from typing import Optional, List
from config.database import Sesion
from models.products import Product as ProductModel
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JwtBearer
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from services.products import Product_Service
from datetime import datetime
from .categories import categorias



products_router = APIRouter()

class products(BaseModel):
    id: Optional[int] = None
    title: str = Field(min_length=5, max_length=15)
    price: float = Field(..., gt=0, description="El precio debe ser mayor a 0")
    description: str = Field(min_length=5, max_length=25)
    images: str = Field
    category_id: int
    updatedAt: datetime = Field(default_factory=datetime.now)
    class Config:
        schema_extra = {
            "example": {
                "id": 1,
                "title": "Mi película",
                "price": "10.00",
                "description": "Descripción del producto",
                "images": "Descripción del producto",
                "category_id": 1
            }
        }

@products_router.get('/products', tags=['products'], response_model=List[products], status_code=200)
def get_product() -> List[products]:
    db = Sesion()
    result = Product_Service(db).get_all_product()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@products_router.get('/products/{id}', tags=['products'], response_model=products)
def get_product(id: int = Path(ge=1, le=2000)) -> products:
    db = Sesion()
    result = Product_Service(db).get_product(id)
    if not result:
         return JSONResponse(status_code=404, content={"message": "No se ha encontrado el producto"})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@products_router.get('/products/', tags=['products'], response_model=List[products], status_code=200)
def get_products_by_category(category: str = Query(min_length=5, max_length=15)) -> List[products]:
    db = Sesion()
    result = Product_Service(db).get_products_by_category(category)
    if not result:
         return JSONResponse(status_code=404, content={"message": "No se ha encontrado el producto"})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))
    

@products_router.post('/products', tags=['products'], response_model=dict, status_code=201)
def create_product(product: products) -> dict:
    db = Sesion()
    Product_Service(db).create_product(ProductModel(**product.dict()))
    return JSONResponse(status_code=201, content={"message": "Se ha registrado el producto"})

@products_router.put('/products/{id}', tags=['products'], response_model=dict, status_code=200)
def update_product(id: int, movie: products)-> dict:
    db = Sesion()
    result = Product_Service(db).get_product(id)
    if not result:
         return JSONResponse(status_code=404, content={"message": "No se ha encontrado el producto"})
    Product_Service(db).update_product(id, movie)
    return JSONResponse(status_code=200, content={"message": "Se ha actualizado el producto"})


@products_router.delete('/products/{id}', tags=['products'], response_model=dict, status_code=200)
def delete_products(id: int) -> dict:
    db = Sesion()
    result = Product_Service(db).get_product(id)
    if not result:
         return JSONResponse(status_code=404, content={"message": "No se ha encontrado el producto"})
    Product_Service(db).delete_product(id)
    return JSONResponse(status_code=200, content={"message": "Se ha eliminado el producto"})